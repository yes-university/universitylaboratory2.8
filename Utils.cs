﻿namespace yesUniversity;

public static class Utils
{
    public static int ToInt(string? text)
    {
        var minus = false;
        var parsed = int.Parse(
            string.Join("",
                (new string[] {"0"})
                .Concat(
                    text?.ToCharArray()
                        .Select((c) =>
                        {
                            var str = c.ToString();
                            if (str != "-") return str;
                            minus = true;
                            return "0";
                        })
                        .Where((s =>
                                {
                                    try
                                    {
                                        int.Parse(s);
                                        return true;
                                    }
                                    catch (Exception e)
                                    {
                                        return false;
                                    }
                                }
                            ))
                    ?? Array.Empty<string>()
                )
            )
        );
        return minus ? -parsed : parsed;
    }

    public static float ToFloatInt(string? text)
    {
        return float.Parse(text ?? "0");
    }

    public static double ToDoubleInt(string? text)
    {
        return Convert.ToDouble(text ?? "0");
    }

    public static int[] GenerateRandomArray(int size, int min, int max)
    {
        var array = new int[size];
        var rnd = new Random();
        for (var i = 0; i < size; i++)
        {
            array[i] = rnd.Next(min, max);
        }

        return array;
    }

    public static double[,] GenerateRandomArray(int size, int min, int max, int size2)
    {
        var array = new double[size, size2];
        var rnd = new Random();
        for (var i = 0; i < size; i++)
        {
            for (var j = 0; j < size2; j++)
            {
                array[i, j] = rnd.Next(min, max);
            }
        }

        return array;
    }

    public static double[,] Transpose(double[,] matrix)
    {
        var w = matrix.GetLength(0);
        var h = matrix.GetLength(1);

        var result = new double[h, w];

        for (var i = 0; i < w; i++)
        {
            for (var j = 0; j < h; j++)
            {
                result[j, i] = matrix[i, j];
            }
        }

        return result;
    }
    public static double[,] MultiplyMatrix(double[,] matrix1, double[,] matrix2)
    {
        var w = matrix1.GetLength(0);
        var h = matrix1.GetLength(1);
        var w2 = matrix2.GetLength(0);
        var h2 = matrix2.GetLength(1);
        
        if (h != w2)
        {
            throw new Exception("Matrix1 is not correct");
        }

        // create result matrix
        var result = new double[w, h2];
        
        // multiply matrix
        for (var i = 0; i < w; i++)
        {
            for (var j = 0; j < h2; j++)
            {
                for (var k = 0; k < h; k++)
                {
                    result[i, j] += matrix1[i, k] * matrix2[k, j];
                }
            }
        }

        // return result
        return result;
    }

    public static void PrintMatrix(double[,] matrix)
    {
        var w = matrix.GetLength(0);
        var h = matrix.GetLength(1);
        // print matrix
        for (var i = 0; i < w; i++)
        {
            for (var j = h -1; j >= 0; j--)
            {
                Console.Write($" {matrix[j, i].ToString(),4}");
            }

            Console.WriteLine();
        }
    }
}