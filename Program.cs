﻿using yesUniversity;
namespace Universitylaboratory2._8
{
    class Program
    {
        private static void Main()
        {
            ConsoleMessageGenerator.GenerateLaboratoryTitle(
                new LaboratoryInfo(
                    "Бродский Егор Станиславович",
                    "ИР-133Б",
                    8,
                    "ЗАСТОСУВАННЯ ДЕЛЕГАТІВ"
                )
            );
            Console.WriteLine();
            TaskChooser.Run(new TaskInfo[]
            {
                new("Завдання 1", () =>
                {

                    Action<Func<bool, int>, char, string> calc = (Func<bool, int> func, char operand, string message) =>
                    {
                        if (operand == '+')
                        {
                            Console.WriteLine(message + " = " + func(false));
                        }
                        else
                        {
                            Console.WriteLine(message + " = " + func(true));
                        }
                       
                    };

                    int firstNumber;
                    calc(
                        (arg) =>
                        {
                            if (!arg)
                            {
                                Console.WriteLine("Введіть перше число:");
                                firstNumber = int.Parse(Console.ReadLine());
                            }
                            else
                            {
                                firstNumber = 1;
                            }
                            
                            Console.WriteLine("Введіть друге число:");
                            var secondNumber = int.Parse(Console.ReadLine());
                            
                            Console.WriteLine("Введіть дугу:");
                            var operand = Console.ReadLine();
                            Console.WriteLine($"Дуга: {operand}");
                            
                            return firstNumber + secondNumber;
                        },
                        '+',
                        "Результат додавання:"
                    );
                    
                    return null;
                }),
            });
        }
    }
}

